flake8
nose
nose-cov
nose-timer
# pytest
amulet
jinja2
juju-wait
python-swiftclient
charms.reactive
charmhelpers
juju-deployer
